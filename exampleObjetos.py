class Impresora:
    modelo=""
    tipo=""
    estado=""
    def __init__(self, modelo, tipo):
        self.modelo = modelo
        self.tipo = tipo
        self.estado = "Listo para imprimir"

    def imprimir(self, documento):
        if self.estado == "Listo para imprimir":
            print(f"Imprimiendo el documento: {documento}")
            self.estado = "Impresión en curso"
        else:
            print("La impresora no está lista para imprimir en este momento.")

    def cambiar_estado(self, nuevo_estado):
        self.estado = nuevo_estado




# Crear un objeto de la clase Impresora
mi_impresora = Impresora("Epson", "Láser")



# Acceder a los atributos de la impresora
print(f"Modelo de la impresora: {mi_impresora.modelo}")
print(f"Tipo de impresora: {mi_impresora.tipo}")
print(f"Estado de la impresora: {mi_impresora.estado}")

# Llamar a los métodos de la impresora
mi_impresora.imprimir("Documento de prueba")
mi_impresora.cambiar_estado("Error de papel")
mi_impresora.imprimir("Otro documento")