#!/usr/bin/python

#
# Simple calculator: add, subs, mult, div
# Can only perform one operation "at a time"
# (well, one of each of the operations"
#
# Copyright Jesus M. Gonzalez-Barahona 2009
# jgb @ gsyc.es
# TSAI and SAT subjects (Universidad Rey Juan Carlos)
# October 2009
#

import webappmulti
#import urlparse from urllib.parse import urlparse
from urllib.parse import urlparse, parse_qs


def decorateHTML(text):
    return ("<html><body>" + text + "</body></html>")


class operation(webappmulti.app):
    """Void class for all operations (sum, sub, mul, div)

    Acts when receiving the following HTTP requests:
     - PUT (arguments in body, as query string): performs the operation
         (and returns the result of the operation)
     - GET: returns the result of the operation

    Methods operate and sign should be overidden by children classes
    """

    def operate(self, oper1, oper2):
        """Placeholder operation, to be extended by children of this class.

        Returns the value of applying it to oper1, oper2."""

        return None

    def sign(self):
        """Placeholder operation, to be extended by children of this class.

        Returns the sign of the operation."""

        return None

    def parse(self, request, rest):

        print("TRAZA1")
        print( request.decode('utf-8'))
        verb = request.decode('utf-8').split(' ', 1)[0]
        #parts = request.decode('utf-8').split('\r\n\r\n', 1)
        parts = request.decode('utf-8').split('?', 1)
        """indice_slash = request.decode('utf-8').find('/')
        indice_signo = request.decode('utf-8').find('?')

        aux=request.decode('utf-8')
        path= aux.substring(indice_slash,indice_signo)
        """
        if len(parts) == 2:
            body = parts[1]
        else:
            body = ""
        return (verb, body)

    def process(self, verb, body):
        print("ENTRA")
        if verb == 'PUT':

            #params = parse_qs(body)
            try:

                # Dividir la cadena en líneas
                lines = body.strip().split('HTTP')

                # Extraer la primera línea que contiene el método, la ruta y la versión HTTP
                first_line = lines[0]


                # Analizar la cadena de consulta
                query_params = parse_qs(first_line)

                # Obtener el valor de oper2 si existe
                self.oper2  = query_params.get('oper2', [''])[0]
                self.oper1 = query_params.get('oper1', [''])[0]

                self.result = self.operate(self.oper1, self.oper2)
                success = True
            except:
                success = False
                (error, message) = ("400 Error",
                                    "Error in parameters for operation")
        elif verb == 'GET':
            success = True
        else:
            success = False
            (error, message) = ("400 Error",
                                "HTTP verb " + verb + " not supported")

        if success:
            return ("200 OK", decorateHTML(str(self.oper1) + self.sign() +
                                           str(self.oper2) +
                                           "=" + str(self.result)))
        else:
            return (error, decorateHTML(message))

    def __init__(self):

        self.oper1 = 0
        self.oper2 = 0
        self.result = 0


class add(operation):
    def operate(self, oper1, oper2):
        return int(oper1) + int(oper2)

    def sign(self):
        return '+'


class sub(operation):
    def operate(self, oper1, oper2):
        return int(oper1) - int(oper2)

    def sign(self):
        return '-'


class mul(operation):
    def operate(self, oper1, oper2):
        return int(oper1) * int(oper2)

    def sign(self):
        return '*'


class div(operation):
    def operate(self, oper1, oper2):
        return oper1 / oper2

    def sign(self):
        return '/'


if __name__ == "__main__":
    addObj = add()
    subObj = sub()
    mulObj = mul()
    divObj = div()
    multiCalc = webappmulti.webApp("localhost", 1234,
                                   {'/add': addObj,
                                    '/sub': subObj,
                                    '/mul': mulObj,
                                    '/div': divObj, })